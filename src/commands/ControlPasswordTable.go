package commands

import (
	"fmt"
	"p-manager/crypter"
	"p-manager/helpers"
)

func IsPasswordTableExists() bool {
	db := helpers.GetDataBaseConnection()

	data := struct {
		Name string
	}{}

	row := db.QueryRow("SELECT name FROM sqlite_master WHERE type='table' AND name=?;", "passwords")

	row.Scan(&data.Name)

	return data.Name == "passwords"
}

func CreateTable(password string) {
	createCommonTableQuery := `CREATE TABLE "passwords" (
		"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"password"	BLOB NOT NULL,
		"key"	TEXT NOT NULL UNIQUE,
		"source"	TEXT,
		"created_at"	INTEGER NOT NULL,
		"updated_at"	INTEGER NOT NULL
	);`

	db := helpers.GetDataBaseConnection()

	_, err := db.Exec(createCommonTableQuery)
	helpers.CheckError(err)

	createConfigTableQuery := `CREATE TABLE "settings" (
		"key"	TEXT NOT NULL,
		"value"	TEXT NOT NULL
	);`

	_, err = db.Exec(createConfigTableQuery)
	helpers.CheckError(err)

	insertPasswordQuery := fmt.Sprintf("INSERT INTO settings (key, value) VALUES('%s', '%s')",
		"password",
		crypter.EncryptDecrypt(helpers.INTERNAL_KEY, password))

	_, err = db.Exec(insertPasswordQuery)

	helpers.CheckError(err)
}
