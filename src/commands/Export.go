package commands

import (
	"encoding/csv"
	"os"
	"p-manager/crypter"
	"p-manager/helpers"
)

func Export() {
	db := helpers.GetDataBaseConnection()

	query := "SELECT id, password, key, source, created_at, updated_at FROM passwords"

	rows, err := db.Query(query)
	helpers.CheckError(err)

	file, err := os.Create("export.csv")
	defer file.Close()
	helpers.CheckError(err)

	writer := csv.NewWriter(file)
	writer.Comma = ';'
	defer writer.Flush()

	passwordKey := GetCryptoKeyFromSettings()

	err = writer.Write([]string{"id", "password", "key", "source", "created_at", "updated_at"})
	helpers.CheckError(err)

	for rows.Next() {
		record := make([]string, 6)
		var rawPassword []byte

		err := rows.Scan(&record[0], &rawPassword, &record[2], &record[3], &record[4], &record[5])
		helpers.CheckError(err)

		record[1] = string(crypter.EncryptDecrypt(passwordKey, string(rawPassword)))

		err = writer.Write(record)
		helpers.CheckError(err)
	}
}
