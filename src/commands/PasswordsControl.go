package commands

import (
	"fmt"
	"math/rand"
	"os"
	"p-manager/crypter"
	"p-manager/helpers"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/rodaine/table"
)

var appPassword string

func AddNewPassword() {
	var key, source, inputPassword string

	if len(os.Args) != 5 {
		color.Green("Input key:")
		fmt.Scanln(&key)

		color.Green("Input source:")
		fmt.Scanln(&source)

		color.Red("Input password:")
		fmt.Scanln(&inputPassword)
	} else {
		key = os.Args[2]
		source = os.Args[3]
		inputPassword = os.Args[4]
	}

	passwordKey := GetCryptoKeyFromSettings()
	password := crypter.EncryptDecrypt(passwordKey, inputPassword)

	currentTime := time.Now().UTC().Unix()

	db := helpers.GetDataBaseConnection()

	stmt, err := db.Prepare("REPLACE INTO passwords (key, source, password, created_at, updated_at) VALUES(?, ?, ?, ?, ?)")

	helpers.CheckError(err)

	_, err = stmt.Exec(key, source, password, currentTime, currentTime)

	helpers.CheckError(err)

	fmt.Println("Successfull saved")
}

func GetPasswordBySource() {
	var source string

	if len(os.Args) == 3 {
		source = os.Args[2]
	} else {
		color.Green("Input source:")
		fmt.Scanln(&source)
	}

	passwordKey := GetCryptoKeyFromSettings()

	db := helpers.GetDataBaseConnection()

	query := "SELECT id, password, key, source, created_at, updated_at FROM passwords where (source like '%' || $1 || '%') or (key like '%' || $2 || '%')"

	rows, err := db.Query(query, source, source)

	helpers.CheckError(err)

	defer rows.Close()

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()

	tbl := table.New("ID", "Password", "Key", "Source", "CreatedAt", "UpdatedAt")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	for rows.Next() {
		var (
			id           int
			rawPassword  []byte
			key          string
			outputSource string
			createdAt    int64
			updatedAt    int64
		)
		err := rows.Scan(&id, &rawPassword, &key, &outputSource, &createdAt, &updatedAt)
		helpers.CheckError(err)

		password := string(crypter.EncryptDecrypt(passwordKey, string(rawPassword)))

		createdAtTime := time.Unix(createdAt, 0)
		updatedAtTime := time.Unix(updatedAt, 0)

		tbl.AddRow(id, password, key, outputSource, createdAtTime, updatedAtTime)
	}

	tbl.Print()
}

func GetPasswordByKey() {
	var key string

	if len(os.Args) == 3 {
		key = os.Args[2]
	} else {
		color.Green("Input key:")
		fmt.Scanln(&key)
	}

	passwordKey := GetCryptoKeyFromSettings()

	db := helpers.GetDataBaseConnection()

	query := fmt.Sprintf("SELECT id, password, source, created_at, updated_at FROM passwords where key='%s'", key)

	row := db.QueryRow(query)

	var (
		id          int
		rawPassword []byte
		source      string
		createdAt   int64
		updatedAt   int64
	)

	err := row.Scan(&id, &rawPassword, &source, &createdAt, &updatedAt)
	helpers.CheckError(err)

	password := string(crypter.EncryptDecrypt(passwordKey, string(rawPassword)))

	createdAtTime := time.Unix(createdAt, 0)
	updatedAtTime := time.Unix(updatedAt, 0)

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()

	tbl := table.New("ID", "Password", "Source", "CreatedAt", "UpdatedAt")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	tbl.AddRow(id, password, source, createdAtTime, updatedAtTime)

	tbl.Print()
}

func DeletePasswordByKey() {
	var key string

	if len(os.Args) == 3 {
		key = os.Args[2]
	} else {
		color.Green("Input key:")
		fmt.Scanln(&key)
	}

	db := helpers.GetDataBaseConnection()

	query := fmt.Sprintf("DELETE FROM passwords where key='%s'", key)

	db.Exec(query)

	color.Green("Successfull deleted")
}

func GeneratePassword() {
	var inputLength, inputCount string

	if len(os.Args) == 4 {
		inputLength = os.Args[2]
		inputCount = os.Args[3]
	} else {
		color.Green("Input length for password:")
		fmt.Scanln(&inputLength)

		color.Yellow("Input count of passwords [default: 10]:")
		fmt.Scanln(&inputCount)
	}

	length, _ := strconv.Atoi(inputLength)

	count := 10

	if inputCount != "" {
		count, _ = strconv.Atoi(inputCount)
	}

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()
	tbl := table.New("ID", "Password")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	for i := 1; i <= count; i++ {
		tbl.AddRow(i, generatePassword(length, 0, 1, 1))
	}

	tbl.Print()
}

func GetCryptoKeyFromSettings() string {

	if appPassword != "" {
		return appPassword
	}

	var inputedPassword string
	fmt.Println("Input your application password:\033[8m")
	fmt.Scan(&inputedPassword)
	fmt.Println("\033[28m")
	db := helpers.GetDataBaseConnection()

	var rawPassword string

	row := db.QueryRow("SELECT value FROM settings WHERE key='password'")

	row.Scan(&rawPassword)

	password := string(crypter.EncryptDecrypt(helpers.INTERNAL_KEY, rawPassword))

	if password != inputedPassword {
		fmt.Println("Wrong password")
		os.Exit(1)
	}

	appPassword = password

	return password
}

func generatePassword(passwordLength, minSpecialChar, minNum, minUpperCase int) string {

	var (
		lowerCharSet   = "abcdedfghijklmnopqrst"
		upperCharSet   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		specialCharSet = "!@#$%&*"
		numberSet      = "0123456789"
		allCharSet     = lowerCharSet + upperCharSet + numberSet
	)

	if minSpecialChar > 0 {
		allCharSet += specialCharSet
	}

	var password strings.Builder

	for i := 0; i < minSpecialChar; i++ {
		random := rand.Intn(len(specialCharSet))
		password.WriteString(string(specialCharSet[random]))
	}

	for i := 0; i < minNum; i++ {
		random := rand.Intn(len(numberSet))
		password.WriteString(string(numberSet[random]))
	}

	for i := 0; i < minUpperCase; i++ {
		random := rand.Intn(len(upperCharSet))
		password.WriteString(string(upperCharSet[random]))
	}

	remainingLength := passwordLength - minSpecialChar - minNum - minUpperCase
	for i := 0; i < remainingLength; i++ {
		random := rand.Intn(len(allCharSet))
		password.WriteString(string(allCharSet[random]))
	}
	inRune := []rune(password.String())
	rand.Shuffle(len(inRune), func(i, j int) {
		inRune[i], inRune[j] = inRune[j], inRune[i]
	})
	return string(inRune)
}
