package commands

import (
	"encoding/csv"
	"errors"
	"io"
	"os"
	"p-manager/crypter"
	"p-manager/helpers"
)

type CsvRow struct {
	Id        string
	Password  string
	Key       string
	Source    string
	CreatedAt string
	UpdatedAt string
}

func Import() {
	if len(os.Args) != 3 {
		helpers.CheckError(errors.New("Error command, commant must contain path to file for import"))
	}

	file, err := os.Open(os.Args[2])
	if err != nil {
		helpers.CheckError(err)
	}

	defer file.Close()

	csvReader := csv.NewReader(file)
	csvReader.Comma = ';'

	for {
		rec, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			helpers.CheckError(err)
		}

		if rec[0] == "id" {
			continue
		}
		row := CsvRow{
			Id:        rec[0],
			Password:  rec[1],
			Key:       rec[2],
			Source:    rec[3],
			CreatedAt: rec[4],
			UpdatedAt: rec[5],
		}
		saveToTable(row)
	}
}

func saveToTable(row CsvRow) {
	db := helpers.GetDataBaseConnection()

	passwordKey := GetCryptoKeyFromSettings()
	password := crypter.EncryptDecrypt(passwordKey, row.Password)

	stmt, err := db.Prepare("REPLACE INTO passwords (key, source, password, created_at, updated_at) VALUES(?, ?, ?, ?, ?)")
	helpers.CheckError(err)
	stmt.Exec(row.Key, row.Source, password, row.CreatedAt, row.UpdatedAt)
}
