package main

import (
	"fmt"
	"log"
	"os"
	"p-manager/commands"

	"github.com/fatih/color"
	"github.com/rodaine/table"
)

func main() {
	if firstRun() {
		os.Exit(0)
	}

	if len(os.Args) >= 2 {
		runCommand(os.Args[1])
	} else {
		commands.GetCryptoKeyFromSettings()
		fmt.Println("Enter commands:")
		for {
			var command string
			fmt.Print("> ")
			fmt.Scanln(&command)
			runCommand(command)
		}
	}
}

func runCommand(command string) {
	switch command {
	case "--help", "help":
		showCommandList()
	case "add", "new":
		commands.AddNewPassword()
	case "delete":
		commands.DeletePasswordByKey()
	case "source", "find":
		commands.GetPasswordBySource()
	case "key":
		commands.GetPasswordByKey()
	case "generate":
		commands.GeneratePassword()
	case "import":
		commands.Import()
	case "export":
		commands.Export()
	case "version", "-v", "--version":
		fmt.Println("Password manager, version 1.0.0.")
	case "exit", "\\q":
		os.Exit(0)
	default:
		fmt.Println("Undefined command: ", command)
	}
}

func firstRun() bool {
	if !commands.IsPasswordTableExists() {
		var password string
		var confirmationPassword string
		fmt.Println("Database is empty, needs create password")
		fmt.Println("Enter Your password:")

		fmt.Scanln(&password)

		fmt.Println("Confirm Your password:")

		fmt.Scanln(&confirmationPassword)

		if password != confirmationPassword {
			log.Fatal("Passwords in not matched")
		}

		commands.CreateTable(password)
		return true
	}
	return false
}

func showCommandList() {
	fmt.Println()
	color.New(color.FgGreen, color.Bold).Println("List commands for p-manager")
	color.New(color.FgYellow, color.Bold).Print("./p-manager")
	color.New(color.FgHiWhite).Print(" {command} {...arguments}\n")

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()

	tbl := table.New("Command", "Arguments", "Description")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	tbl.AddRow("add", "{key} {source} {password}", "add password to database")
	tbl.AddRow("source", "{source}", "search password by source")
	tbl.AddRow("key", "{key}", "show password by key")
	tbl.AddRow("generate", "{length} {count [default:10]}", "generate passwords")
	tbl.AddRow("export", "", "exporting decrypted password to csv file")
	tbl.AddRow("import", "{path to file}", "import decrypted password to current base, and crypted. Attention!!! Records with same key will be replaced")

	tbl.Print()
	fmt.Println()

	color.New(color.FgHiRed, color.Bold).Print("!!! ")
	color.New(color.FgWhite).Print("The ")
	color.New(color.FgHiGreen, color.Bold).Print("'key' ")
	color.New(color.FgWhite).Println("parameter is unique, if you have used it for record before, the information will be overwritten")
}
