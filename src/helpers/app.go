package helpers

import (
	"database/sql"
	"fmt"
	"math/rand"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

var INTERNAL_KEY = "golangisverygoodprogramminglanguage"

var dbConnection *sql.DB

func GetDataBaseConnection() *sql.DB {
	if dbConnection == nil {
		return createDBConnection()
	}
	return dbConnection
}

func GetRandomString(length int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, length)
	rand.Read(b)
	return fmt.Sprintf("%x", b)[:length]
}

func createDBConnection() *sql.DB {
	dbConnection, err := sql.Open("sqlite3", "file:data.dat")
	CheckError(err)
	dbConnection.SetMaxOpenConns(2)
	return dbConnection
}
