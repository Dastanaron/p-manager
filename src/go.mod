module p-manager

go 1.16

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/rodaine/table v1.0.1 // indirect
)
