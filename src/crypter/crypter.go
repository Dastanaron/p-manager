package crypter

func EncryptDecrypt(key string, input string) []byte {
	keyLength := len(key)
	output := []byte{}
	for i := range input {
		output = append(output, input[i]^key[i%keyLength])
	}
	return output
}
