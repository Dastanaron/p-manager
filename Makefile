.PHONY: all clean build

APP_NAME=p-manager

all: prod

prod: clean compile

## default run
run:
	cd src; go run . $(ARGS)

## check race condition
race:
	cd src; go run -race .

## default build
build:	
	cd src; go build -o ../build/${APP_NAME} .

## production build (strip the debugging information)
compile:
	cd src;	GOOS=linux GOARCH=amd64 go build -ldflags "-X 'p-manager/helpers.INTERNAL_KEY=${APP_KEY}' -s -w" -o ../build/${APP_NAME} .
	cd src; env GOOS=darwin GOARCH=amd64 go build -ldflags "-X 'p-manager/helpers.INTERNAL_KEY=${APP_KEY}' -s -w" -o ../build/${APP_NAME}_mac_os .

## clear cache and remove builds
clean:
	go clean
	rm -f build/${APP_NAME}
	rm -f build/${APP_NAME}_x86
