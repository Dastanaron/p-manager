PASSWORD MANAGER
==========================

[donate](https://qiwi.com/n/MISIT616)

This is programm for your passwords. Programm working with SQLite database. All password is crypted in database.

Used Libraries
--------------------------

|   link                    	|   version	|
|---                        	|---	    |
|   github.com/fatih/color	    |   1.13.0	|
|   github.com/mattn/go-sqlite3	|   1.14.9	|
|   github.com/rodaine/table	|   1.0.1	|